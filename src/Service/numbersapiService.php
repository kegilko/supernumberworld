<?php

namespace App\Service;

/**
 * Class numbersapiService
 * @package App\Service
 */
class numbersapiService
{
    /**
     *
     */
    const API_URL = "http://numbersapi.com/";

    /**
     * @param int $data
     * @param string $suffix
     * @return bool|string
     */
    private function curlToApi(int $data, string $suffix = ""){
        $ch = curl_init();
        curl_setopt($ch,
            CURLOPT_URL,
            self::API_URL . $data . $suffix);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    /**
     * @param int $data
     * @return bool|string
     */
    public function getYearFact(int $data){
        return $this->curlToApi($data, "/year");
    }

    /**
     * @param int $data
     * @return bool|string
     */
    public function getNumberFact(int $data){
        return $this->curlToApi($data);
    }

}