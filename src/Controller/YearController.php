<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Service\numbersapiService;

class YearController extends AbstractController
{
    /**
     * @Route("/year", name="year_get", methods={"GET"})
     */
    public function index()
    {
        return $this->render('year/index.html.twig');
    }

    /**
     * @Route("/year", name="year_post", methods={"POST"})
     * @param Request $req
     * @param numbersapiService $nbrAPI
     * @return Response
     */
    public function process(Request $req, numbersapiService $nbrAPI)
    {
        $datas = $req->request->all();
        return $this->render('year/index.html.twig', [
            "annee"        => $datas["annee_choisie"],
            "resultat_api"  => $nbrAPI->getYearFact($datas["annee_choisie"])
        ]);
    }
}
