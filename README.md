# Application Super Number World sous Symfony 5

## Déploiement

### Prerequis

    php >= v7.2.5
    
### Installation

    composer install
    
Et si vous switchez sur la branche "advanced", il faut passer les commandes suivantes :

    composer install
    php bin/console doctrine:database:create # le .env est configuré pour gérer une bdd sqlite simple
    php bin/console doctrine:migrations:migrate # car une bdd est utilisée pour stocker des stats
    
### Démarrer un server de dev local 
    
Depuis Symfony 5, pour démarrer un server de dev local, il est nécessaire d'installer l'exécutable symfony ( marche à suivre ici : https://symfony.com/download )

Une fois ceci installé, exécuter depuis la racine du projet un simple :

    symfony server:start

## Utilisation

Sur la branche master, deux pages sont disponibles :
    
    /year
    
et

    /number 

Sur la branche advanced, seule une page est dispo (url '/')

## Par rapoprt au cours 

La branche master est là pour illustrer le code présenté dans le cours

La branche advanced est une amélioration de la branche master :
- du code a été factorisé ; fusion des controleurs en un seul amélioré, fusion des templates en un seul amélioré 
- création d'entités et du service stat afin de compter le nombre de recherche 
- amélioration du service numbersAPI 
- des commentaires mis un peu partout